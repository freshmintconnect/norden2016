<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Fresh2016
 */

get_header(); ?>

<section id="hero">
		<?php query_posts('post_type=hp-slide')?>
		<?php if (have_posts()) : ?>
		<div class="slide-wrap">
			<?php while (have_posts()) : the_post(); 
			$slidebg = get_field('slide_bg');
			?>
			
			<div class="slide" id="<?php the_title(); ?>">
				<div class="slide-bg" style="background-image:url(<?php echo $slidebg; ?>);"></div>
				<div class=" container">
					<div class="inner">
						<div class="slide-img"> <?php the_post_thumbnail(); ?></div>
						<div class="slide-cnt">
							<h1 class="slide-hl">Oops. This Content Can't Be Found</h1>
							<div class="sitesearch">
								<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<?php endif; ?>
	</section>

<?php
get_footer();
