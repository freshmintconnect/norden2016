<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fresh2016
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('page-cnt'); ?>>
	<header class="entry-hdr">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-hdr -->

	<div class="entry-cnt">
		<?php the_content();?>
	</div><!-- .entry-cnt -->

</article><!-- #post-## -->
