<div class="btn-block cf">
	<?php 
		$allowed_classnames = array(
		    1 => 'full',
		    2 => 'half',
		    3 => 'third',
		);
		$number_of_cols = count( get_sub_field( 'block_links' ) );
		$classname_to_use = $allowed_classnames[1];
		if ( array_key_exists( $number_of_cols , $allowed_classnames ) ) {
		    $classname_to_use = $allowed_classnames[$number_of_cols];
		};
		if( have_rows('block_links') ): 
		echo '<ul>';
		while( has_sub_field( 'block_links' ) ) : 
		$btntxt = get_sub_field ('btn_txt');
		$urlarray = get_sub_field ('btn_url');
		$btnurl = array_values($urlarray)[0];
		
		?>
		<li class="<?php echo esc_attr( $classname_to_use ); ?>"><a href="<?php echo $btnurl ?>" class="btn"><?php echo $btntxt ?> </a></li>

	<?php endwhile; echo '</ul>'; endif;?>
</div><!--/btn-block-->