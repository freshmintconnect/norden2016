<div class="sec-title"><h2 class="widgettitle container">News &amp; Upcoming Events</h2></div>

<section id="recent-news" class="container ">
	<?php 
		$blog_query = $wp_query;
		query_posts('category_name=news&showposts=3');
	    while (have_posts()) : the_post();
		$count = 0;
		get_template_part( 'partials/loop', 'listpost');
		$counter++;
		endwhile; 
		
		wp_reset_query();
	?>
</section><!--container-->