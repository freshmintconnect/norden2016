<div class="block prod-block">
	<div class="block-tmb tmb"><img src="<?php the_sub_field('block_img'); ?>" /></div>
	<div class="block-cnt">
		<h3 class="entry-title"><?php the_sub_field('block_title'); ?></h3>
		<div class="entry-cnt"><?php the_sub_field('block_txt'); ?></div>
	</div>
	<?php get_template_part( 'partials/loop', 'btnblock' ); ?>

</div> 
