<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fresh2016
 */

?>
<article <?php post_class('listpost cf'); ?>> 
	
	<a href="<?php the_permalink(); ?>" class="blnk">
		<span class="entry-tmb">
			<?php the_post_thumbnail('fresh-medium'); ?>
		</span>
		<span class="entry-cnt">
			<span class="entry-title"><?php the_title(); ?></span>
			<span class="entry-txt"><?php the_excerpt(); ?></span>
		</span>
	</a>
</article><!-- #post-## -->