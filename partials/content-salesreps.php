<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fresh2016
 */

?>

<article id="<?php echo strtolower(str_replace(' ', '', get_the_title())); ?>" <?php post_class('post-cnt rep-cnt'); ?>>

	<div class="entry-cnt">
		<div class="rep-states col">
			<h4>Service Areas:</h4>
			<ul>
			<?php
			$posttags = get_the_tags();
			if ($posttags) {
			 	
			  foreach($posttags as $tag) {
			    echo '<li>' . $tag->name . '</li>'; 
			  }
			}
			?>
			</ul>
		</div>
		<div class="rep-fields col">
			<ul>
				<?php 
					$repurl = get_field('rep_url');
					$repemail = get_field('rep_email');
				?>
				<li class="rep-title"><h3 class="entry-title"><a href="<?php echo $repurl ?>"><?php the_title(); ?></a></h3></li>
				<?php if(get_field('rep_name')) :
				echo '<li class="rep-name"><span class="label">Contact:</span>' . get_field('rep_name') . '</li>'; endif; ?>
				<li class="rep-add"><?php the_field('rep_add'); ?>
				<li class="rep-email"><h3><a href="mailto:<?php echo $repemail ?>?subject=Norden Sales Request"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send Email</a></h3></li>
			</ul>
		</div>
	</div><!-- .entry-cnt -->

</article><!-- #post-## -->
