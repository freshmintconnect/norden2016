<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fresh2016
 */

?>

	</div><!-- #content -->
	<section  id="catalog-request">
		<div class="container inner">
			<?php echo do_shortcode('[gravityform id="2" title="true" description="false" ajax="true"]'); ?>
		</div>
	</section>
	
	<div class="ftr-blog">
	<?php get_template_part( 'partials/loop', 'ftrblog' ); ?>
	</div>
	
	
	<footer id="site-footer" role="contentinfo">
		<div class="ftr-widgets container">
			<?php if ( !function_exists('dynamic_sidebar')
			|| !dynamic_sidebar('sidebar-1') ) : ?>
			<?php endif; ?>
		</div>
		
		
		<div class="ftr-btm">
			<section id="connect" class="widget container">
				<div class="entry-title td"><h2>Corporate Office</h2></div>
				<div class="entry-cnt td">
					<div class="address">
						<h3>Address</h3> 
						<?php echo do_shortcode('[text-blocks id="address" plain="1"]'); ?>
					</div>
					<div class="phone">
						<h3>Contact</h3> 
						<ul>
							<li><?php echo do_shortcode('[text-blocks id="phone" plain="1"]'); ?></li>
					
							<li><?php echo do_shortcode('[text-blocks id="fax" plain="1"]'); ?></li>
							<li class="em"><a class="btn" href="/contact">Email Us</a></li>
						</ul>
					</div>
					<div class="entry-map">
						<a class="gmap" target="_blank" href="https://www.google.com/maps/place/Norden+Millimeter/@38.6784124,-120.91671,13z/data=!4m5!3m4!1s0x0:0x9f46059375e9aa8c!8m2!3d38.6763017!4d-120.8786012"><span class="map-link"><i class="fa fa-map-marker gmap-icn"></i>Open In Google Maps</span></a>
					</div>
				</div>
			</section>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
