jQuery(document).ready(function($) { 
	$('.nlsu input').focus(function() {
       $(this).prev('span').text('');
	});
// 	$('.sub-menu').wrap('<div class="mega"></div>');	
	$('.prodtable').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching":     false,
        responsive: true
    } );
	$('a[href*=\\#]:not([href=\\#])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
  $('.dd-trigger').addClass('closed');
  $('.dd-trigger').click(function(e){
    	e.stopPropagation();
    	if(this.getElementsByTagName("ul")[0].style.display ==="block") {
    		$(this).find("ul").slideUp();
    		$(this).removeClass('open');
    		$(this).addClass('closed');
    	} else {
    		$(this).children(":first").slideDown();
    		$(this).removeClass('closed');
    		$(this).addClass('open');
    	}
    });
    $( ".mega-searchtoggle a" ).click(function() {
	  $( ".hdrsearch " ).toggle( "slide", function() {
	    // Animation complete.
	  });
	});
	
});

