<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fresh2016
 */

get_header(); ?>
	
	
	<section id="hero">
		<?php query_posts('post_type=hp-slide')?>
		<?php if (have_posts()) : ?>
		<div class="slide-wrap">
			<?php while (have_posts()) : the_post(); 
			$slidebg = get_field('slide_bg');
			?>
			
			<div class="slide" id="<?php the_title(); ?>">
				<div class="slide-bg" style="background-image:url(<?php echo $slidebg; ?>);"></div>
				<div class=" container">
					<div class="inner">
						<div class="slide-img"> <?php the_post_thumbnail(); ?></div>
						<div class="slide-cnt">
							<h1 class="slide-hl"> <?php the_field('slide_hl'); ?> </h1>
							<div class="sitesearch">
								<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<?php endif; ?>
	</section>
		
	<div id="aboutus" class="sec-wrap au-blocks">
		<?php if( have_rows('prod_blocks') ): ?> 
			<section id="products" class="sec-cnt blocks container">
				<?php while ( have_rows('prod_blocks') ) : the_row();  ?>
			 		<?php get_template_part( 'partials/loop', 'block' ); ?>
		 		<?php endwhile; ?>
			</section>
		<?php  endif; ?>
		

		
	</div><!-- #primary -->
	
<?php
get_footer();
