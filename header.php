<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
$telstring = show_text_block('phone', true); 
function clean($string) {
   $telstring = str_replace('-', '', $telstring); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $telstring); // Removes special chars.
}
?>	

<div id="page" class="site">
	<a class="skip-link sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'fmd_' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		
		<div class="hdr-top container">
			<div class="col col-l"> <h2><?php echo do_shortcode('[text-blocks id="tagline-left" plain="1"]'); ?></h2></div>
			
			<div id="site-branding">
				<a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?> </a>
			</div><!-- .site-branding -->
			
			<div class="col col-r"> <h2><?php echo do_shortcode('[text-blocks id="tagline-right" plain="1"]'); ?></h2></div>
		</div>
		
		<div class="hdr-nav">
			<nav  class="main-nav container" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'nav-main', 'menu_id' => 'hdrnav' ) ); ?>
				<div class="hdrsearch">
					<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
				</div>
			</nav><!-- #site-navigation -->
<!--
			<ul class="global-nav">
				<li class="hide-th"><a class="menu-btn"><i class="fa fa-bars"></i> <span class="sr-only">Menu</span></a></li>
				<li ><a href="#connect"><i class="fa fa-envelope-square" aria-hidden="true"></i> <span class="sr-only">Email Us</span></a>
				</li>
				<li class="hide-th"><a href="tel:<?php echo clean($telstring);?>"><i class="fa fa-phone-square" aria-hidden="true"></i> <span class="sr-only">Call Us</span></a></li>
				
			</ul>
-->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">