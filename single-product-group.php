<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fresh2016
 */

get_header(); ?>

	<div id="primary" class="content-area ">
		<main id="main" class="site-main container" role="main">
		<header class="page-hdr">
			<div class="hdr-tmb block-tmb"> <?php the_post_thumbnail(); ?>  </div>
			<div class="hdr-cnt">
				<?php the_title( '<h1 class="page-hl">', '</h1>' ); ?>
				<ul class="prod-table-links">
				<?php if( have_rows('prod_tables') ): while ( have_rows('prod_tables') ) : the_row();  ?>
			 			<li><a href="#<?php echo strtolower(str_replace(' ', '-', get_sub_field('prod_table_title'))); ?>"><?php the_sub_field('prod_table_title'); ?></a></li>	 			<?php endwhile; endif; wp_reset_query();?>
			 	</ul> 
			</div>
		</header><!-- .entry-hdr -->

		
		
	 	
		
		
		<?php if( have_rows('prod_tables') ): while ( have_rows('prod_tables') ) : the_row();  ?>
	 		<div class="prod-table" id="<?php echo strtolower(str_replace(' ', '-', get_sub_field('prod_table_title'))); ?>">
 				<h3 class="table-title"><?php the_sub_field('prod_table_title'); ?></h3>
 				<?php the_sub_field('prod_table_data'); ?>
	 		</div> 
		<?php endwhile; endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
