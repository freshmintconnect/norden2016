<?php
/*
Template Name: Sidebar Content
*/
get_header(); ?>

	<div id="primary" class="content-area inner-page sb-page">
			<main id="main" class="site-main container" role="main">
				<?php while (have_posts()) : the_post(); ?>
					
					
					<div class="page-main">
						<?php get_template_part( 'partials/content' ); ?>
					</div>
					<aside class="page-sb">
						<?php if(get_field('cb_gallery')) :?>
						<div class="sb-gal">
							<?php 
							$image_ids = get_field('cb_gallery', false, false);
							$shortcode = '[gallery soliloquy="true" ids="' . implode(',', $image_ids) . '"]';
							echo do_shortcode( $shortcode );
							?>
						</div>
						<?php endif; ?>
						
						<div class="entry-cnt">
							<?php the_field('cb_sb'); ?>
						</div>
						
					</aside>
				<?php endwhile; ?>
			</main>
	</div><!-- #primary -->

<?php get_footer(); ?>

