<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fresh2016
 */

get_header(); ?>

	<div id="primary" class="content-area inner-page sales-page container">
		<main id="main" class="site-main " role="main">
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">Domestic Sales</h1>
			</header><!-- .page-header -->
			
			<section class="rep-wrap">
				<aside class="state-list sticker">
					<?php echo do_shortcode('[us_wp_map]'); ?>
					<h3>Choose Your Area</h3>
				</aside>
				<div class="rep-list">
					<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'partials/content', 'salesreps', get_post_format() );
					endwhile;
					endif; ?>
				</div>
			</section>
		</main><!-- #main -->
		
		<script>
			jQuery(document).ready(function($) { 

        $('.sticker').sticky('scrolling_container_selector');
    
			});
			
			</script>
		
	</div><!-- #primary -->

<?php
get_footer();
