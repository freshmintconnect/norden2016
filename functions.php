<?php
/**
 * Fresh2016 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Fresh2016
 */

if ( ! function_exists( 'fmd__setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function fmd__setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Fresh2016, use a find and replace
	 * to change 'fmd_' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'fmd_', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'nav-main' => esc_html__( 'Header', 'fmd_' ),
		'nav-ftr' => esc_html__( 'Footer', 'fmd_' ),
	) );
/*
	add_filter( 'wp_nav_menu_items', 'contact_menu_item', 10, 2 );
	function contact_menu_item ( $items, $args ) {
	    if ($args->theme_location == 'hdr-r') {
	        $items .= '<li><a class="connect-lnk" href="#connect"><i class="fa fa-envelope-square" aria-hidden="true"></i></a></li>';
	    }
	    return $items;
	}
*/

	
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'link',
	) );
 
}
endif;
add_action( 'after_setup_theme', 'fmd__setup' );

add_image_size( 'htmb', 110, 85, true );
add_image_size( 'fresh-medium', 240, 175, true );
add_image_size( 'fresh-large', 1440, 500, true );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function fmd__widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar', 'fmd_' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'fmd_' ),
		'id'            => 'blog-sb',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'fmd__widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function fmd__scripts() {
	wp_enqueue_style( 'fmd_fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'fmd_style', get_stylesheet_uri() );
/*
	wp_enqueue_style( 'pushy-css', get_stylesheet_directory_uri() . '/css/pushy.css', array(), '1.0', 'all' );
	wp_enqueue_script( 'pushy-js', get_stylesheet_directory_uri() . '/js/pushy.js', array(), '1.0', 'all' );
*/
	wp_enqueue_style( 'datatables-css', 'https://cdn.datatables.net/t/dt/dt-1.10.11,r-2.0.2/datatables.min.css', array(), '1.0', 'all' );
	wp_enqueue_script( 'datatables-js', 'https://cdn.datatables.net/t/dt/dt-1.10.11,r-2.0.2/datatables.min.js', array(), '1.0', 'all' );
	wp_enqueue_script( 'sticky', get_stylesheet_directory_uri() . '/js/jquery.stickyelement.js', array(), '1.0', 'all' );

	wp_enqueue_script( 'fmd', get_stylesheet_directory_uri() . '/js/init.js	', array(), '1.0', 'all' );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'fmd__scripts' );
add_filter('widget_text', 'do_shortcode');

require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
//require get_template_directory() . '/inc/custom-header.php';
//require get_template_directory() . '/inc/customizer.php';

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );