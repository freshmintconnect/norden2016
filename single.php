<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fresh2016
 */

get_header(); ?>

	<div id="primary" class="content-area inner-page blog-page">
		<?php
			if ( has_post_thumbnail()) : 
			$bigtmb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$liltmb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
			$bigurl = $bigtmb['0']; 
			$lilurl = $liltmb['0']; 
			?>
			<main id="main" class="site-main container hastmb" role="main">
				<div class="page-tmb mtmb hide-t">
					<div class="tmbwrap"><img src="<?php echo $lilurl; ?>" /></div>
				</div>
				<div class="page-tmb ftmb hide-s">
					<div class="tmbwrap"><img src="<?php echo $bigurl; ?>" /></div>
				</div>
			<?php else : ?>		
			<main id="main" class="site-main container" role="main">
			<?php 
				endif;
				while ( have_posts() ) : the_post();
					get_template_part( 'partials/content' );
				endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
